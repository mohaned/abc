# -*- encoding: utf-8 -*-

import xmlrpclib
import socket
import os
import time
import datetime
import base64
import re

from openerp.osv import fields, osv, orm
from openerp import tools
from openerp import netsvc
from openerp import tools, _
import logging

try:
    from gdback import upload_utility
except ImportError:
    raise ImportError('Cannot import google drive upload !')

_logger = logging.getLogger(__name__)


def execute(connector, method, *args):
    res = False
    try:
        res = getattr(connector, method)(*args)
    except socket.error, e:
        raise e
    return res


addons_path = tools.config['addons_path'] + '/auto_backup/DBbackups'


class db_backup(osv.Model):
    _name = 'db.backup'

    def get_db_list(self, cr, user, ids, host='localhost', port='8069', context={}):
        uri = 'http://' + host + ':' + port
        conn = xmlrpclib.ServerProxy(uri + '/xmlrpc/db')
        db_list = execute(conn, 'list')
        return db_list

    def _get_db_name(self, cr, uid, vals, context=None):
        attach_pool = self.pool.get("ir.logging")
        dbName = cr.dbname
        return dbName

    _columns = {
        # Columns local server
        'host': fields.char('Host', size=100, required='True'),
        'port': fields.char('Port', size=10, required='True'),
        'name': fields.char('Database', size=100, required='True', help='Database you want to schedule backups for'),
        'bkp_dir': fields.char('Backup Directory', size=100, help='Absolute path for storing the backups',
                               required='True'),
        'autoremove': fields.boolean('Auto. Remove Backups',
                                     help="If you check this option you can choose to automaticly remove the backup after xx days"),
        'daystokeep': fields.integer('Remove after x days',
                                     help="Choose after how many days the backup should be deleted. For example:\nIf you fill in 5 the backups will be removed after 5 days.",
                                     required=True),
    }

    _defaults = {
        # 'bkp_dir' : lambda *a : addons_path,
        'bkp_dir': '/odoo/backups',
        'host': lambda *a: 'localhost',
        'port': lambda *a: '8069',
        'name': _get_db_name,
        'daystokeepsftp': 30,
    }

    def _check_db_exist(self, cr, user, ids):
        for rec in self.browse(cr, user, ids):
            db_list = self.get_db_list(cr, user, ids, rec.host, rec.port)
            if rec.name in db_list:
                return True
        return False

    _constraints = [
        (_check_db_exist, _('Error ! No such database exists!'), [])
    ]

    def schedule_backup(self, cr, user, context={}):
        conf_ids = self.search(cr, user, [])
        confs = self.browse(cr, user, conf_ids)
        for rec in confs:
            db_list = self.get_db_list(cr, user, [], rec.host, rec.port)
            if rec.name in db_list:
                try:
                    if not os.path.isdir(rec.bkp_dir):
                        os.makedirs(rec.bkp_dir)
                except:
                    raise
                # Create name for dumpfile.
                bkp_file = '%s_%s.dump' % (time.strftime('%d_%m_%Y_%H_%M_%S'), rec.name)
                file_path = os.path.join(rec.bkp_dir, bkp_file)
                uri = 'http://' + rec.host + ':' + rec.port
                conn = xmlrpclib.ServerProxy(uri + '/xmlrpc/db')
                bkp = ''
                try:
                    bkp = execute(conn, 'dump', tools.config['admin_passwd'], rec.name)
                except:
                    logger.notifyChannel('backup', netsvc.LOG_INFO,
                                         "Couldn't backup database %s. Bad database administrator password for server running at http://%s:%s" % (
                                             rec.name, rec.host, rec.port))
                    continue
                fp = open(file_path, 'wb')
                bkp = base64.decodestring(bkp)
                fp.write(bkp)
                fp.close()

                lp = '/odoo/log.log'
                # Write to google drive !
                loglp = open(lp, 'a')

                try:
                    loglp.write('Start uploading !' + '\n')
                    upload_utility(file_path)
                    loglp.write('End uploading !' + '\n')
                    print('Uploaded to google drive !')
                except Exception as e:
                    loglp.write('Error  !' + '\n')
                    loglp.write(str(e) + '\n')
                    print('Cannot upload to google drive')
            else:
                logger.notifyChannel('backup', netsvc.LOG_INFO,
                                     "database %s doesn't exist on http://%s:%s" % (rec.name, rec.host, rec.port))


db_backup()
