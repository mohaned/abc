from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import os
from sys import argv, exit

def list_files(drive):
	file_list = drive.ListFile({'q': "'root' in parents and trashed=false"}).GetList()
	for file1 in file_list:
	  print 'title: %s, id: %s' % (file1['title'], file1['id'])

def get_google_drive():
	gauth = GoogleAuth()

	if gauth.credentials is None:
	    gauth.LocalWebserverAuth()
	elif gauth.access_token_expired:
	    gauth.Refresh()
	else:
	    gauth.Authorize()

	d = GoogleDrive(gauth)

	return d

def upload_file(d, file_path):
	# print(''.join(['*' * 10, 'Uploading your file: %s' % (file_path,), '*' * 10]))

	f = d.CreateFile({'title': file_path.split('/')[-1]})
	f.SetContentFile(file_path)
	f.Upload()

def upload_utility(file_path):
	gdrive = get_google_drive()
	upload_file(gdrive, file_path)


def main():

	if len(argv) < 2:
		print('Please provide me with path')
		exit(1)
		
	file_name = argv[1]

	if not os.path.isfile(file_name):	
		print('Please provide me with a valid file !')
		exit(1)	


	# gdrive = get_google_drive()
	# upload_file(gdrive, file_name)
	# print(''.join(['*' * 10, 'listing all files', '*' * 10]))
	# list_files(gdrive)

	upload_utility(file_name)

# main()